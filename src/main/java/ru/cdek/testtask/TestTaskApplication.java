package ru.cdek.testtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.time.ZoneOffset;
import java.util.TimeZone;

@SpringBootApplication
public class TestTaskApplication {
	// todo Костыль, но чтобы весь бэк работал с UTC, надо чтобы время окружения было в UTC
	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC));
	}

	public static void main(String[] args) {
		SpringApplication.run(TestTaskApplication.class, args);
	}
}
