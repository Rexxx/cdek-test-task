package ru.cdek.testtask.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.cdek.testtask.exception.BadRequestException;
import ru.cdek.testtask.model.MessageResponse;
import ru.cdek.testtask.model.auth.ERole;
import ru.cdek.testtask.model.auth.JwtInfo;
import ru.cdek.testtask.model.auth.Role;
import ru.cdek.testtask.model.auth.User;
import ru.cdek.testtask.model.auth.UserSignInInfo;
import ru.cdek.testtask.model.auth.UserSignUpInfo;
import ru.cdek.testtask.repository.RoleRepository;
import ru.cdek.testtask.repository.UserRepository;
import ru.cdek.testtask.security.AuthTokenFilter;
import ru.cdek.testtask.security.JwtUtils;
import ru.cdek.testtask.service.UserDetailsImpl;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private AuthenticationManager authenticationManager;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder encoder;
    private JwtUtils jwtUtils;

    public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository,
                          RoleRepository roleRepository, PasswordEncoder encoder, JwtUtils jwtUtils) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/signin")
    public JwtInfo authenticateUser(@Valid @RequestBody UserSignInInfo signInInfo,
                                              HttpServletResponse response) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(signInInfo.getUsername(), signInInfo.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        JwtInfo jwtInfo = new JwtInfo(jwt, userDetails.getId(), userDetails.getUsername(),
                userDetails.getEmail(), roles);
        Cookie cookie = new Cookie("token", jwtInfo.getAccessToken());
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        response.addCookie(cookie);

        return jwtInfo;
    }

    @PostMapping("/signout")
    public ResponseEntity<?> deauthenticateUser(HttpServletResponse response) {
        AuthTokenFilter.resetJwt(response);
        return ResponseEntity.ok(new MessageResponse("User logged out."));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserSignUpInfo signUpInfo) {
        if (userRepository.existsByUsername(signUpInfo.getUsername())) {
            throw new BadRequestException("Error: Username is already taken!");
        }

        if (userRepository.existsByEmail(signUpInfo.getEmail())) {
            throw new BadRequestException("Error: Email is already in use!");
        }

        User user = new User(signUpInfo.getUsername(),
                signUpInfo.getEmail(),
                encoder.encode(signUpInfo.getPassword()));

        Set<String> strRoles = signUpInfo.getRole();
        Set<Role> roles = new HashSet<>();

        strRoles.forEach(role -> {
            switch (role.toLowerCase()) {
                case "courier":
                    Role courier = roleRepository.findByName(ERole.ROLE_COURIER)
                            .orElseThrow(() -> new BadRequestException("Error: Role is not found."));
                    roles.add(courier);
                    break;
                case "operator":
                    Role operator = roleRepository.findByName(ERole.ROLE_OPERATOR)
                            .orElseThrow(() -> new BadRequestException("Error: Role is not found."));
                    roles.add(operator);
                    break;
                default:
                    throw new BadRequestException("Error: Role is not found.");
            }
        });

        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}
