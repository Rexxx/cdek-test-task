package ru.cdek.testtask.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.cdek.testtask.exception.BadRequestException;
import ru.cdek.testtask.exception.NotFoundException;
import ru.cdek.testtask.model.callTask.CallTask;
import ru.cdek.testtask.model.callTask.CallTaskSearchInfo;
import ru.cdek.testtask.model.callTask.CallTaskUpdateInfo;
import ru.cdek.testtask.model.callTask.Views;
import ru.cdek.testtask.service.CallTaskService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/call-task")
public class CallTaskController {
    private final CallTaskService callTaskService;

    public CallTaskController(CallTaskService callTaskService) {
        this.callTaskService = callTaskService;
    }

    @PostMapping
    @PreAuthorize("hasRole('COURIER')")
    @JsonView(Views.OrderNumberCreationDateTime.class)
    public CallTask create() {
        return callTaskService.create();
    }

    @GetMapping
    @PreAuthorize("hasRole('OPERATOR')")
    public List<CallTask> search(@Valid CallTaskSearchInfo searchInfo) {
        return callTaskService.search(searchInfo);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('OPERATOR')")
    public CallTask update(@PathVariable("id") CallTask taskFromDb, @RequestBody CallTaskUpdateInfo updateInfo) {
        if (taskFromDb == null) {
            throw new NotFoundException("CallTask with specified id doesn't exist.");
        }

        try {
            return callTaskService.update(taskFromDb, updateInfo);
        }
        catch (IllegalArgumentException ex) {
            throw new BadRequestException(ex.getMessage());
        }
    }
}
