package ru.cdek.testtask.model.auth;

public enum ERole {
    ROLE_COURIER,
    ROLE_OPERATOR
}
