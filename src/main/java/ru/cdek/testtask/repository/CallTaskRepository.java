package ru.cdek.testtask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.cdek.testtask.model.callTask.CallTask;

import java.time.ZonedDateTime;
import java.util.List;

public interface CallTaskRepository extends JpaRepository<CallTask, Long> {
    @Query(value = "SELECT t FROM CallTask t WHERE t.creationDateTime BETWEEN :from AND :to")
    List<CallTask> findBetweenCreationDates(ZonedDateTime from, ZonedDateTime to);
}
