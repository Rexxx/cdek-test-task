package ru.cdek.testtask.service;

import org.springframework.stereotype.Service;
import ru.cdek.testtask.model.callTask.CallTask;
import ru.cdek.testtask.model.callTask.CallTaskSearchInfo;
import ru.cdek.testtask.model.callTask.CallTaskUpdateInfo;
import ru.cdek.testtask.repository.CallTaskRepository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CallTaskService {
    private final CallTaskRepository callTaskRepository;

    public CallTaskService(CallTaskRepository callTaskRepository) {
        this.callTaskRepository = callTaskRepository;
    }

    public CallTask create() {
        CallTask task = new CallTask();
        ZonedDateTime currentTime = DateTimeUtils.getCurrentTime();
        task.setCreationDateTime(currentTime);
        task.setCompleted(false);
        return callTaskRepository.save(task);
    }

    public List<CallTask> search(CallTaskSearchInfo searchInfo) {
        if (searchInfo == null) {
            throw new NullPointerException("CallTaskSearchInfo can't be null");
        }

        ZonedDateTime from = DateTimeUtils.resetZoneOffset(searchInfo.getFrom());
        ZonedDateTime to = DateTimeUtils.resetZoneOffset(searchInfo.getTo());
        List<CallTask> searchResult = callTaskRepository.findBetweenCreationDates(from, to);

        if (searchInfo.getOrderNumber() != null) {
            searchResult = searchResult.stream()
                    .filter(t -> t.getOrderNumber().equals(searchInfo.getOrderNumber()))
                    .collect(Collectors.toList());
        }

        return searchResult;
    }

    public CallTask update(CallTask task, CallTaskUpdateInfo updateInfo) {
        if (task == null) {
            throw new NullPointerException("CallTask can't be null");
        }

        if (updateInfo == null) {
            throw new NullPointerException("CallTaskUpdateInfo can't be null");
        }

        ZonedDateTime currentTime = DateTimeUtils.getCurrentTime();
        ZonedDateTime deliveryDateTime = DateTimeUtils.resetZoneOffset(updateInfo.getDeliveryDateTime());

        if (deliveryDateTime.compareTo(currentTime) <= 0) {
            throw new IllegalArgumentException("DeliveryDateTime in CallTaskUpdateInfo can't be less than current time.");
        }

        task.setDeliveryDateTime(deliveryDateTime);
        task.setCompleted(true);
        return callTaskRepository.save(task);
    }
}
