package ru.cdek.testtask.service;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class DateTimeUtils {
    public static ZonedDateTime getCurrentTime() {
        return ZonedDateTime.now(ZoneOffset.UTC);
    }

    public static ZonedDateTime resetZoneOffset(ZonedDateTime dateTime) {
        return ZonedDateTime.ofInstant(dateTime.toInstant(), ZoneOffset.UTC);
    }
}
