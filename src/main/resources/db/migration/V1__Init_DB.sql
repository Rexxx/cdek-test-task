create sequence ct_seq start 1 increment 1;
create sequence role_seq start 1 increment 1;
create sequence usr_seq start 1 increment 1;

create table call_tasks (
    order_number int8 not null,
    creation_date_time timestamp with time zone,
    delivery_date_time timestamp with time zone,
    is_completed boolean,
    primary key (order_number)
);

create table roles (
    id int4 not null,
    name varchar(20),
    primary key (id)
);

create table user_roles (
    user_id int8 not null,
    role_id int4 not null,
    primary key (user_id, role_id)
);

create table users (
    id int8 not null,
    email varchar(50),
    password varchar(64),
    username varchar(20),
    primary key (id)
);

alter table if exists users add constraint UKr43af9ap4edm43mmtq01oddj6 unique (username);
alter table if exists users add constraint UK6dotkott2kjsp8vw4d0m25fb7 unique (email);
alter table if exists user_roles add constraint FKh8ciramu9cc9q3qcqiv4ue8a6 foreign key (role_id) references roles;
alter table if exists user_roles add constraint FKhfh9dx7w3ubf1co1vdev94g3f foreign key (user_id) references users;

insert into roles(id, name) values(nextval ('role_seq'), 'ROLE_COURIER');
insert into roles(id, name) values(nextval ('role_seq'), 'ROLE_OPERATOR');

insert into users (email, password, username, id)
    values ('courier@cdek.ru', '$2a$10$S0lMDhSPAlVeEZff1H11een5oxILsKrZNd2.eQjfBQjtb4ZeDono2', 'courier', nextval ('usr_seq'));
insert into user_roles (user_id, role_id) values (currval('usr_seq'), 1);

insert into users (email, password, username, id)
    values ('operator@cdek.ru', '$2a$10$CrVUaJCIlH1R6KZyF1FWfOXqDQc3v/geNqoCdF.BlHmM.H/0vOjbK', 'operator', nextval ('usr_seq'));
insert into user_roles (user_id, role_id) values (currval('usr_seq'), 2);

insert into call_tasks (creation_date_time, delivery_date_time, is_completed, order_number) values ('2020-04-03T11:15:00.000+00:00', '2020-04-07T16:30:00.000+00:00', true, nextval ('ct_seq'));
insert into call_tasks (creation_date_time, delivery_date_time, is_completed, order_number) values ('2020-04-04T16:22:00.000+00:00', null, false, nextval ('ct_seq'));
insert into call_tasks (creation_date_time, delivery_date_time, is_completed, order_number) values ('2020-04-04T09:08:00.000+00:00', null, false, nextval ('ct_seq'));
insert into call_tasks (creation_date_time, delivery_date_time, is_completed, order_number) values ('2020-04-05T12:54:00.000+00:00', null, false, nextval ('ct_seq'));
insert into call_tasks (creation_date_time, delivery_date_time, is_completed, order_number) values ('2020-04-05T13:33:00.000+00:00', null, false, nextval ('ct_seq'));
